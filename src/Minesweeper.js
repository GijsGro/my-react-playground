import React, {useEffect, useState} from 'react';
import './Minesweeper.css';

const ROWS = 9;
const COLS = 9;
const MINES = 10;
let FLAGS = 0;
let GAMEOVER = false;

const Minesweeper = () => {
    const [board, setBoard] = useState([]);
    const [openCells, setOpenCells] = useState([]);
    const [userFlags, setUserFlags] = useState([]);
    const [flagMode, setMode] = useState(false);

    useEffect(() => {
        initializeBoard();
    }, []);

    const initializeBoard = () => {
        setOpenCells(Array(ROWS).fill(null).map(() => Array(COLS).fill(false)));
        setUserFlags(Array(ROWS).fill(null).map(() => Array(COLS).fill(false)));
        FLAGS = 0;
        GAMEOVER = false;
        const newBoard = Array(ROWS).fill(null).map(() => Array(COLS).fill(0));
        let minesPlaced = 0;
        while (minesPlaced < MINES) {
            const row = Math.floor(Math.random() * ROWS);
            const col = Math.floor(Math.random() * COLS);
            if (newBoard[row][col] === 0) {
                newBoard[row][col] = 'm';
                minesPlaced++;
            }
        }
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                if (newBoard[i][j] !== 'm') newBoard[i][j] = countAdjacentMines(newBoard, i, j);
            }
        }
        setBoard(newBoard);
    };

    function cascadeOpen(newOpenCells, row, col) {
        newOpenCells[row][col] = true;
        if (userFlags[row][col]) {
            userFlags[row][col] = false;
            FLAGS--;
        }
        if (board[row][col] === 0) {
            for (let i = -1; i <= 1; i++) {
                for (let j = -1; j <= 1; j++) {
                    if (i === 0 && j === 0) continue;

                    const newRow = row + i;
                    const newCol = col + j;

                    if (newRow >= 0 && newRow < ROWS && newCol >= 0 && newCol < COLS) {
                        if (!newOpenCells[newRow][newCol]) {
                            cascadeOpen(newOpenCells, newRow, newCol);
                        }
                    }
                }
            }
        }
    }

    let winByOpening = (() => {
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                if (board[i][j] !== 'm' && !openCells[i][j]) return false;
            }
        }
        return true;
    })

    let winByFlags = (() => {
        if (MINES - FLAGS !== 0) return false;
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                if (userFlags[i][j] && board[i][j] !== 'm') return false;
            }
        }
        return true;
    })

    const handleCellClick = (row, col) => {
        if (!GAMEOVER) {
            if (flagMode && !openCells[row][col]) {
                const newUserFlags = [...userFlags];
                newUserFlags[row][col] = !newUserFlags[row][col];
                newUserFlags[row][col] ? FLAGS++ : FLAGS--;
                setUserFlags(newUserFlags);
            } else {
                if (board[row][col] === 'm') {
                    const newOpenCells = [...openCells];
                    for (let i = 0; i < ROWS; i++) {
                        for (let j = 0; j < COLS; j++) {
                            if (board[i][j] === 'm' && !userFlags[i][j]) newOpenCells[i][j] = true;
                        }
                    }
                    setOpenCells(newOpenCells);
                    GAMEOVER = true;
                    alert('Game Over!');
                } else {
                    const newOpenCells = [...openCells];
                    cascadeOpen(newOpenCells, row, col);
                    setOpenCells(newOpenCells);
                }
            }
            if (winByOpening() || winByFlags()) {
                let newOpenCells = [...openCells];
                for (let i = 0; i < ROWS; i++) {
                    for (let j = 0; j < COLS; j++) {
                        if (!userFlags[i][j] && board[i][j] === 'm') newOpenCells[i][j] = true;
                    }
                }
                setOpenCells(newOpenCells);
                GAMEOVER = true;
                alert('You win!');
            }
        }
    };

    const countAdjacentMines = (b, row, col) => {
        let count = 0;

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (i === 0 && j === 0) continue;

                const newRow = row + i;
                const newCol = col + j;

                if (newRow >= 0 && newRow < ROWS && newCol >= 0 && newCol < COLS) {
                    if (b[newRow][newCol] === 'm') {
                        count++;
                    }
                }
            }
        }

        return count;
    };

    return (
        <div className="App">
            <div className="d-flex justify-content-center">
                <div className="flex-row">
                    <div className="flex-col">
                        <p>mines<br/>
                            Left:</p>
                    </div>
                    <div className="flex-col">
                        <button className={"icoBox"}>
                            {GAMEOVER ? 0 : (MINES - FLAGS)}
                        </button>
                    </div>
                </div>
                <div className="flex-row px-5">
                    <div className="flex-col">
                        <p>Toggle<br/>
                            flag:</p>
                    </div>
                    <div className="flex-col">
                        <button className={"icoBox flagBox"}
                                onClick={() => setMode(!flagMode)}
                                style={{backgroundColor: flagMode ? "yellow" : "lightgray"}}>
                            F
                        </button>
                    </div>
                </div>
                <div className="flex-row">
                    <div className="flex-col">
                        <p>Reset<br/>
                            game:</p>
                    </div>
                    <div className="flex-col">
                        <button className={"icoBox"}
                                style={{backgroundColor: "lightgray"}}
                                onClick={() => initializeBoard()}>
                            R
                        </button>
                    </div>
                </div>
            </div>
            <div className="board">
                {board.map((row, rowIndex) => (
                    <div key={rowIndex} className="row">
                        {row.map((cell, colIndex) => (
                            <div
                                key={colIndex}
                                className={`cell ${openCells[rowIndex][colIndex] ? (cell === 'm' ? 'mine' : '') : (userFlags[rowIndex][colIndex] ? (GAMEOVER ? (cell === 'm' ? 'flagBox flagBombWin' : 'flagBox badFlag') : 'flagBox flag') : 'closed')}`}
                                onClick={() => handleCellClick(rowIndex, colIndex)}
                            >
                                {openCells[rowIndex][colIndex] ? (cell !== 0 && (cell === 'm' ? 'F' : cell)) : (
//                                    userFlags[rowIndex][colIndex] ? ((GAMEOVER && cell === 'm') ? '󰔓' : '󰈻') : ''
                                    userFlags[rowIndex][colIndex] ? 'F' : ''
                                )}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        </div>
    );
}
export default Minesweeper;

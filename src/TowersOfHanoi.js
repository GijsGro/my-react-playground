import React, {useEffect, useState} from 'react';
import './TowersOfHanoi.css';

let inputValue = 5;
let diskColors = Array.from({length: inputValue}, () => null);

const TowersOfHanoi = () => {
    const [towers, setTowers] = useState([
        Array.from({length: inputValue}, (_, i) => (i + 1)),
        [],
        []
    ]);

    const setColors = (inputValue) => {
        let colorList = [];
        let colorArray = [
            "#FF4500",
            "#09a209",
            "#0a51ed",
            "#FFD700",
            "#9445df",
            "#04ffeb",
            "#d07d06",
            "#72ff3a",
        ];
        for (let i = 0; i < inputValue; i++) {
            colorList[i] = colorArray[i % colorArray.length];
        }
        return colorList;
    };

    function addValue(inputValue) {
        setIsGameWon(false)
        inputValue++
        setTowers([Array.from({length: inputValue}, (_, i) => (i + 1)), [], []])
        setSelectedTower(null)
        diskColors = setColors(inputValue)
        return inputValue
    }

    function rmValue(inputValue) {
        setIsGameWon(false)
        inputValue--
        setTowers([Array.from({length: inputValue}, (_, i) => (i + 1)), [], []])
        setSelectedTower(null)
        diskColors = setColors(inputValue)
        return inputValue
    }

    diskColors = setColors(inputValue);

    const [selectedTower, setSelectedTower] = useState(null);
    const [isGameWon, setIsGameWon] = useState(false);

    useEffect(() => {
        if (!isGameWon) {
            if (towers[1].length === inputValue || towers[2].length === inputValue) {
                setIsGameWon(true);
            } else {
                setIsGameWon(false);
            }
        }
    }, [towers, isGameWon]);

    const moveDisk = (from, to) => {
        const newTowers = [...towers];
        if (newTowers[to].length === 0 || newTowers[from][0] <= newTowers[to][0]) {
            const disk = newTowers[from].shift();
            newTowers[to].reverse();
            newTowers[to].push(disk);
            newTowers[to].reverse();
            setTowers(newTowers);
            setSelectedTower(null)
        } else {
            setSelectedTower(to)
        }

    };

    const selectTower = (towerIndex) => {
        if (isGameWon) {
            setIsGameWon(false)
            setTowers([Array.from({length: inputValue}, (_, i) => (i + 1)), [], []])
        }
        if (selectedTower === null) {
            if (towers[towerIndex].length > 0) setSelectedTower(towerIndex);
        } else {
            moveDisk(selectedTower, towerIndex);
        }
    };

    return (
        <div className="App">
            <div className="d-flex justify-content-center align-content-center">
                <div className="flex-row">
                    <div className="flex-col">
                        <p>Number<br/>of disks:</p>
                    </div>
                    <div className="flex-col">
                        <button className={"towerSizeChange"}>
                            {inputValue}
                        </button>
                    </div>
                </div>
                <div className="flex-row px-5">
                    <div className="flex-col">
                        <p>Change<br/>number:</p>
                    </div>
                    <div className="d-inline">
                        <button className={"towerSizeChange addValue"}
                                style={{backgroundColor: "lightgray"}}
                                onClick={() => inputValue = addValue(inputValue)}>
                            +
                        </button>
                    </div>
                    <div className="d-inline">
                        <button className={"towerSizeChange rmValue"}
                                style={{backgroundColor: "lightgray"}}
                                onClick={() => inputValue = rmValue(inputValue)}>
                            –
                        </button>
                    </div>
                </div>
                <div className="flex-row">
                    <div className="flex-col">
                        <p>Reset<br/>
                            game:</p>
                    </div>
                    <div className="flex-col">
                        <button className={"towerSizeChange"}
                                style={{backgroundColor: "lightgray"}}
                                onClick={() => {
                                    setIsGameWon(false)
                                    setTowers([Array.from({length: inputValue}, (_, i) => (i + 1)), [], []])
                                }}>
                            󰑙
                        </button>
                    </div>
                </div>
            </div>
            <div className="towers">
                {towers.map((tower, index) => (
                    <div
                        key={index}
                        className={`tower ${selectedTower === index ? 'selected' : ''}`}
                        onClick={() => selectTower(index)}
                    >
                        <div key={index} className={"pole"}></div>
                        {tower.map((disk, index) => (
                            <div key={index} className={`disk`}
                                 style={{
                                     zIndex: tower.length - index,
                                     width: ((0.2 + 0.7 * (disk / inputValue)) * 100) + "%",
                                     backgroundColor: diskColors[disk - 1]
                                 }}
                            >
                                {disk}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
            {isGameWon && alert('You did it!')}
        </div>
    );
};

export default TowersOfHanoi;

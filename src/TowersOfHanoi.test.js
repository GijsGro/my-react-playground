import {render, screen} from '@testing-library/react';
import TowersOfHanoi from "./TowersOfHanoi";

test('renders number of disks', () => {
    render(<TowersOfHanoi/>);
    const linkElement = screen.getByText(/Number of disks:/);
    expect(linkElement).toBeInTheDocument();
});

test('renders initial disk numbers', () => {
    render(<TowersOfHanoi/>)
    const elements = []
    for (let i = 0; i < elements.length; i++) {
        elements[i] = screen.getByText((i + 1) + "")
        if (elements[i].length <= 1) expect(elements[i]).toBeInTheDocument()
        else {
            for (let j = 0; j < elements[i].length; j++) {
                expect(elements[i][j]).toBeInTheDocument()
            }
        }
    }
});

test('renders the initial disk-count on the disk and in the header', () => {
    render(<TowersOfHanoi/>)
    const initialValue = screen.getAllByText("5")
    expect(initialValue).toHaveLength(2)
})

test('renders buttons to add and remove a disk', () => {
    render(<TowersOfHanoi/>)
    const addBtn = screen.getByText("+")
    const removeBtn = screen.getByText("–")
    expect(addBtn).toBeInTheDocument()
    expect(removeBtn).toBeInTheDocument()
});

import React from 'react';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {createRoot} from "react-dom/client";
import Minesweeper from "./Minesweeper";
import TowersOfHanoi from "./TowersOfHanoi";
import {Accordion, AccordionItem} from "react-bootstrap";
import SnakeGame from "./SnakeGame";

createRoot(
    document.getElementById('root')
).render(
    <container className="d-inline-flex justify-content-start item">
        <Accordion defaultActiveKey="0" className="flex" style={{"minWidth": "540px"}}>
            <AccordionItem eventKey="0">
                <Accordion.Header>
                    Towers Of Hanoi &nbsp;
                </Accordion.Header>
                <Accordion.Body style={{
                    "minHeight": "500px"
                }}>
                    <TowersOfHanoi/>
                </Accordion.Body>
            </AccordionItem>
            <AccordionItem eventKey="1">
                <Accordion.Header>
                    Minesweeper &nbsp;
                </Accordion.Header>
                <Accordion.Body style={{
                    "minHeight": "500px"
                }}
                >
                    <Minesweeper/>
                </Accordion.Body>
            </AccordionItem>
            <AccordionItem eventKey="2">
                <Accordion.Header>
                    Snake &nbsp;
                </Accordion.Header>
                <Accordion.Body style={{
                    "minHeight": "500px"
                }}>
                    <div className={"col m-2 p-1"}>
                        <SnakeGame size={450}/>
                    </div>
                </Accordion.Body>
            </AccordionItem>
        </Accordion>
    </container>
)